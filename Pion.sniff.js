var _0x8196 = ["ogario", "", "#999999", "#998c36", "mapOffsetX", "mapOffset", "mapOffsetY", "mapOffsetFixed", "cursorX", "clientX", "innerW", "canvasScale", "viewScale", "playerX", "cursorY", "clientY", "innerH", "playerY", "length", "playerCellsMass", "playerMass", "playerBestMass", "virColors", "oppColors", "oppRings", "showStatsSTE", "playerMinMass", "playerMaxMass", "selectBiggestCell", "STE", "floor", "0", "#", "color2Hex", "push", "smallerCellsCache", "STECellsCache", "biggerCellsCache", "biggerSTECellsCache", "ogarioCtx", "prototype", "_fillRect", "fillRect", "_strokeText", "strokeText", "textStroke", "apply", "_fillText", "fillText", "Leaderboard", "displayLeaderboard", "leaderboardIndex", "leaderboardHTML", ":teams", "gameMode", ".", "indexOf", "&gt;", "replace", "&lt;", "&amp;", "#ffaaaa", "fillStyle", "<span class=\"me my-color\">", "</span>", "clanTag", "<span class=\"clan-color\">", "<span>"];
! function() {
    window.ogario= {
        play: !1,
        spectate: !1,
        gameMode: "",
        clanTag: "",
        playerNick: "",
        playerColor: null,
        playerX: 0,
        playerY: 0,
        playerCells: [],
        playerCellsMass: [],
        foodCache: [],
        virusesCache: [],
        CellsCache: [],
        ballPool: [],
        leaderBoardInfo: [],
        mapEvent: [],
        playerMass: 0,
        playerMinMass: 0,
        playerMaxMass: 0,
        playerBestMass: null,
        STE: null,
        mapSize: 14142,
        mapOffset: 7071,
        mapOffsetX: 0,
        mapOffsetY: 0,
        mapOffsetFixed: false,
        mapMinX: -7071,
        mapMinY: -7071,
        mapMaxX: 7071,
        mapMaxY: 7071,
        zoomValue: 1,
        zoomResetValue: 0,
        zoomSpeedValue: 0.9,
        viewScale: 1,
        canvasScale: 1,
        innerW: 0,
        innerH: 0,
        cursorX: 0,
        cursorY: 0,
        clientX: 0,
        clientY: 0,
        mapLeft: 0,
        mapRight: 0,
        mapUp: 0,
        mapDown: 0,
        menuHeight: 580,
        cellMemOffset: null,
        nameMemOffset: null,
        idOffset: null,
        idMemOffset: null,
        gameCtx: null,
        leaderboardHTML: "",
        leaderboardIndex: 1,
        gloabalAlpha: 1,
        cellsAlpha: 0.9,
        skinsAlpha: 0.7,
        virusColor: "#999999",
        virusStrokeColor: "#999999",
        virusAlpha: 0.6,
        foodColor: "#998c36",
        autoZoom: false,
        autoHideCellsInfo: true,
        autoHideFood: true,
        showVirusCount: false,
        hideMyName: true,
        hideMyMass: false,
        hideEnemiesMass: false,
        customSkins: true,
        hsloSkins: true,
        showCustomSkins: true,
        myTransparentSkin: false,
        myCustomColor: false,
        transparentCells: false,
        transparentViruses: true,
        transparentSkins: false,
        showFood: true,
        rainbowFood: false,
        oppColors: false,
        oppRings: false,
        virColors: false,
        splitRange: false,
        virusesRange: false,
        textStroke: false,
        cursorTracking: true,
        enableCommander: true,
        showStatsSTE: true,
        popchat: true,
        selectBiggestCell: true,
        hideSmallBots: false,
        pause: false,
        isEnableTeammateIndicator: true,
        chatbox: true,
        targetSpec: false,
        selectTarget1: false,
        selectTarget2: false,
        showVirusCount: false,
        displayLeaderboard: null,
        customDraw: null,
        drawGrid: null,
        setOppColor: null,
        setVirusColor: null,
        setVirusStrokeColor: null,
        getString: null,
        getCustomSkin: null,
        focusX: null,
        focusY: null,
        teammateIndicator: null,
        teammateIndicatorShowSize: 200,
        oppColorMap: [{
            type: 1
            , color: "#d3d3d3"//灰
        }, {
            type: 2
            , color: "#76FF03"//綠
        }, {
            type: 4
            , color: "#2196F3"//藍
        }, {
            type: -1
            , color: "#FF9800"//橘
        }, {
            type: -2
            , color: "#FD0000"//紅
        }, {
            type: -4
            , color: "#FFFFFF"//白
        }],
        setMapCoords: function(_0x331fx1, _0x331fx2, _0x331fx3, _0x331fx4, _0x331fx5, _0x331fx6) {
            _0x331fx6 - _0x331fx5 == 24 && _0x331fx3 - _0x331fx1 > 14e3 && _0x331fx4 - _0x331fx2 > 14e3 && (this[_0x8196[4]] = this[_0x8196[5]] - _0x331fx3, this[_0x8196[6]] = this[_0x8196[5]] - _0x331fx4, this[_0x8196[7]] = true);
            this.mapLeft = this.mapMinX - this.mapOffsetX;
            this.mapUp = this.mapMinY - this.mapOffsetY;
            this.mapRight = this.mapMaxX - this.mapOffsetX;
            this.mapDown = this.mapMaxY - this.mapOffsetY;
            
        },
        /*setCursorPosition: function() {
            this[_0x8196[8]]  = ~~(this[_0x8196[9]]  - this[_0x8196[10]] / 2) * this[_0x8196[11]] / this.viewScale + this[_0x8196[13]];
            this[_0x8196[14]] = ~~(this[_0x8196[15]] - this[_0x8196[16]] / 2) * this[_0x8196[11]] / this.viewScale + this[_0x8196[17]];
        },*/
        calculateMass: function() {
            if (0 != this[_0x8196[19]][_0x8196[18]]) {
                if ((this[_0x8196[20]] > this[_0x8196[21]] || !this[_0x8196[21]]) && (this[_0x8196[21]] = this[_0x8196[20]]), this[_0x8196[22]] || this[_0x8196[23]] || this[_0x8196[24]] || this[_0x8196[25]]) {
                    this[_0x8196[26]] = this[_0x8196[19]][0], this[_0x8196[27]] = this[_0x8196[19]][0];
                    for (var _0x331fx1 = 1; _0x331fx1 < this[_0x8196[19]][_0x8196[18]]; _0x331fx1++) {
                        this[_0x8196[19]][_0x331fx1] < this[_0x8196[26]] ? this[_0x8196[26]] = this[_0x8196[19]][_0x331fx1] : this[_0x8196[19]][_0x331fx1] > this[_0x8196[27]] && (this[_0x8196[27]] = this[_0x8196[19]][_0x331fx1])
                    }
                };
                if (this[_0x8196[25]]) {
                    var _0x331fx2 = this[_0x8196[28]] ? this[_0x8196[27]] : this[_0x8196[26]];
                    this[_0x8196[29]] = _0x331fx2 > 35 ? Math[_0x8196[30]](_0x331fx2 * (1e3 > _0x331fx2 ? 0.35 : 0.38)) : null
                }
            }
        },
        color2Hex: function(e) {
            var _0x331fx2 = e.toString(16);
            return 1 == _0x331fx2[_0x8196[18]] ? _0x8196[31] + _0x331fx2 : _0x331fx2
        },
        rgb2Hex: function(r, g, b) {
            return "#" + this.color2Hex(r) + this.color2Hex(g) + this.color2Hex(b)
        },
        setAutoHideCellsInfo: function(e) {
            return 40 >= e || this.viewScale < 0.5 && 500 > e && e < 24 / this.viewScale ? !0 : !1
        }
    };
    CanvasRenderingContext2D.prototype.ogarioCtx = !1;
    CanvasRenderingContext2D.prototype._fillRect = CanvasRenderingContext2D.prototype.fillRect;
    CanvasRenderingContext2D.prototype.fillRect = function() {};
    CanvasRenderingContext2D.prototype._strokeText = CanvasRenderingContext2D.prototype.strokeText;
    CanvasRenderingContext2D.prototype.strokeText = function() {
        window.ogario.textStroke && this._strokeText.apply(this, arguments)
    };
    CanvasRenderingContext2D.prototype._fillText = CanvasRenderingContext2D.prototype.fillText;
    CanvasRenderingContext2D.prototype.fillText = function() {
        var text = arguments[0];
        return "Leaderboard" === text ? (window.ogario[_0x8196[50]] && window.ogario[_0x8196[50]](), window.ogario[_0x8196[51]] = 1, void((window.ogario[_0x8196[52]] = ""))) : _0x8196[53] !== window.ogario[_0x8196[54]] && 0 == text[_0x8196[56]](window.ogario[_0x8196[51]] + _0x8196[55]) && window.ogario[_0x8196[51]] < 11 ? (text[_0x8196[58]](/&/g, _0x8196[60])[_0x8196[58]](/</g, _0x8196[59])[_0x8196[58]](/>/g, _0x8196[57]), (text = text.length ? text : "An unnamed cell"), window.ogario.leaderBoardInfo[window.ogario.leaderboardIndex] = text, window.ogario[_0x8196[52]] += _0x8196[61] == this[_0x8196[62]] ? _0x8196[63] + text + _0x8196[64] : 3 == text[_0x8196[56]](window.ogario[_0x8196[65]]) || 4 == text[_0x8196[56]](window.ogario[_0x8196[65]]) && 10 == window.ogario[_0x8196[51]] ? _0x8196[66] + text + _0x8196[64] : _0x8196[67] + text + _0x8196[64], void(window).ogario[_0x8196[51]]++) : void(this)[_0x8196[47]].apply(this, arguments)
    }
}()
