// ==UserScript==
// @name         Pion Plus Lite
// @namespace    Pion
// @version      1.9.0
// @description  Private extention
// @author       Pion,szymy
// @match        http://agar.io/*
// @updateURL    https://bitbucket.org/pionxzh/pionplus/raw/d69ff6c30e956caccfd9f525688023850321d65e/pion.user.js
// @run-at       document-start
// @grant        GM_xmlhttpRequest
// @connect      agar.io
// ==/UserScript==

var ppJS = '<script src="https://bitbucket.org/pionxzh/pionplus/raw/d69ff6c30e956caccfd9f525688023850321d65e/PionPlus.js"></script>';
var ppSniffJS = '<script src="https://bitbucket.org/pionxzh/pionplus/raw/d69ff6c30e956caccfd9f525688023850321d65e/Pion.sniff.js"></script>';
var ppCSS = '<link href="https://bitbucket.org/pionxzh/pionplus/raw/d69ff6c30e956caccfd9f525688023850321d65e/pion.min.css" rel="stylesheet"></link>';
var cpickerJS = '<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.0/js/bootstrap-colorpicker.min.js"></script>';
var cpickerCSS = '<link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.0/css/bootstrap-colorpicker.min.css" rel="stylesheet"></link>';
var toastrJS = '<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>';
var toastrCSS = '<link href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"></link>';
var perfect = '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.8/js/min/perfect-scrollbar.jquery.min.js"></script>';

function inject(page) {
    var _page = page.replace("</head>", cpickerCSS + toastrCSS + ppCSS + cpickerJS + toastrJS + ppSniffJS + perfect + "</head>");
    _page = _page.replace("agario.core.js", "");
    _page = _page.replace("</body>", ppJS + "</body>");
    return _page;
}
window.stop();
document.documentElement.innerHTML = "";
GM_xmlhttpRequest({
    method : "GET",
    url : "http://agar.io/",
    onload : function(e) {
        var doc = inject(e.responseText);
        document.open();
        document.write(doc);
        document.close();
    }
});